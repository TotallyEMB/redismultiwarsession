package com.models;

import java.io.Serializable;

public class User implements Serializable {

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    private String description;
    private String name;

}
