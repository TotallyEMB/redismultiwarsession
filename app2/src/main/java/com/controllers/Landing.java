package com.controllers;


import com.models.User;
import com.session.SessionRetrieval;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.Calendar;

@Controller
@RequestMapping(value = "/")
public class Landing {

    @RequestMapping(value = "/app2", method = RequestMethod.GET)
    public String landingContent(final Model model) {
        HttpSession httpSession = SessionRetrieval.getSession();
        //check redis for session... if empty make a new session and store in redis
        if (httpSession == null) {
            httpSession = SessionRetrieval.getNewSession();
            final User user = new User();
            user.setName("AppTwoJoe");
            user.setDescription("session started on app2");
            httpSession.setAttribute("USER", user);
            httpSession.setAttribute("TIME_SET", Calendar.getInstance().getTime());
        }

        model.addAttribute("MODE_USER", httpSession.getAttribute("USER"));
        model.addAttribute("TIME_CREATED", httpSession.getAttribute("TIME_SET"));

        return "home";
    }
}
